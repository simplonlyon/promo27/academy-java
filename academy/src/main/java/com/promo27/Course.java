package com.promo27;

import java.util.ArrayList;
import java.util.List;

public class Course {

    private String name;
    private List<Student> students;
    private Professor prof ;


    public Course(String name, Professor prof) {
        this.name = name;
        this.prof = prof;
        this.students = new ArrayList<>();
    }

  

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public Professor getProf() {
        return prof;
    }

    public void setProf(Professor prof) {
        this.prof = prof;
    }

   
    public void addStudent(Student student) {
        students.add(student);
 
     }

    
}
