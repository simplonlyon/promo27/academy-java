package com.promo27;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


        Professor javaProf = new Professor("Jean");
        Professor pythonProf = new Professor("Mathieu");
        Professor phpProf = new Professor("Amine");



        System.out.println("Veuillez choisir un cours");
        System.out.println("1 - JavaX");
        System.out.println("2- Python");
        System.out.println("3- Php");


        Scanner scanner = new Scanner(System.in);
        int choix = scanner.nextInt();


        String coursChoisi = "";
        Professor profChoisi = null;

        if (choix == 1) {
            coursChoisi = "JavaX";
            profChoisi = javaProf;
        } else if (choix == 2) {
            coursChoisi = "Python";
            profChoisi = phpProf;
        } else if (choix == 3) {
            coursChoisi = "Php";
            profChoisi = pythonProf;
        } else {
            System.out.println("Choix invalide");
        
        }

        System.out.println("Veuillez entrer votre nom :");
        String nomEtudiant = scanner.next();

        Student etudiant = new Student(nomEtudiant);
        Course cours = new Course(coursChoisi, profChoisi);
        cours.addStudent(etudiant);

        System.out.println("Bonjour " + nomEtudiant + " vous êtes inscrit(e) au cours de " + coursChoisi + " avec le prof " + profChoisi.getName());
   
        scanner.close(); 

    }
}
