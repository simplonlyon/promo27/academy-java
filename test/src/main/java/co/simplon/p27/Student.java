package co.simplon.p27;

public class Student {
    private String nom;
    private int age;
    private int promo;
    private int note;


    public int getNote() {
        return note;
    }



    public void setNote(int note) {
        this.note = note;
    }



    public String getNom() {
        return nom;
    }



    public void setNom(String nom) {
        this.nom = nom;
    }



    public int getAge() {
        return age;
    }



    public void setAge(int age) {
        this.age = age;
    }



    public int getPromo() {
        return promo;
    }



    public void setPromo(int promo) {
        this.promo = promo;
    }



    public Student(String nom, int age, int promo, int note) {
        this.nom = nom;
        this.age = age;
        this.promo = promo;
        this.note = note;
    }



    public void bonjour(){
        System.out.println("Bonjour, je m'appelle " + nom + " et j'ai " + age + " et je suis dans la promo " + promo);
    }
    

    public void atttaquet(String nomP){
        System.out.println("Bonjour Mr"+ nomP);
    }
}
