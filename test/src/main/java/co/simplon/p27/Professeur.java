package co.simplon.p27;

public class Professeur {

    public String nom;
    public String matiere;
    public int classe;



    public String getNom() {
        return nom;
    }


    public void setNom(String nom) {
        this.nom = nom;
    }


    public String getMatiere() {
        return matiere;
    }


    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }


    public int getClasse() {
        return classe;
    }


    public void setClasse(int classe) {
        this.classe = classe;
    }


    public Professeur(String nom, String matiere, int classe) {
        this.nom = nom;
        this.matiere = matiere;
        this.classe = classe;
    }


    public void presenter(){
            System.out.println("Je suis le professeur " + nom + ", j'enseigne la matière de " + matiere);
    }



}
